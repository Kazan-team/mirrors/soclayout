#!/bin/sh

echo 'THIS IS OUT OF DATE DO NOT USE'
exit 1

# initialise/update the pinmux submodule
git submodule update --init --remote

# makes symlinks to alliance
./mksyms.sh

# generates the io pads needed for ioring.py
make pinmux

# copies over a "dummy" core (empty)
cp non_generated/ls180.vst .

# starts the build.
make lvx

