from Hurricane import DebugSession

#DebugSession.addToTrace( katana.getCell().getNet( 'cu_issue_i' ) )
#DebugSession.addToTrace( katana.getCell().getNet( 'cu_wr_rel_o(2)' ) )
#DebugSession.addToTrace( katana.getCell().getNet( 'xer_so_ok' ) )
#DebugSession.addToTrace( katana.getCell().getNet( 'core.libresocsim_libresoc_interface0_dat_w(28)' ) )
#DebugSession.addToTrace( katana.getCell().getNet( 'core.libresocsim_libresoc_interface1_dat_r(8)' ) )
# Antenna @(4567,1932)
#DebugSession.addToTrace( katana.getCell().getNet( 'core.subckt_12948_test_issuer.subckt_0_ti.subckt_3774_core.subckt_4317_fus.subckt_6_mul0.subckt_1030_alu_mul0.mul_pipe1_ra_bit19_hfns_3' ) )
# M3 spacing (no need to expand)
# [ERROR] Overlap in <VerticalTrack [6428] METAL3 @4242.48um [-0.52um:5108.38um] [111/128]> between:
#  <id:7115121 Vertical core.subckt_12948_test_issuer.subckt_0_ti.subckt_3774_core.subckt_4310_dec_duiuvu.dec_duiuvu_in2_sel_bit3_hfns_1 METAL3 [4242.48um 3934.92um] [4242.48um 3958.02um] 0.38um rpD:1 -----CG----W------bt- [3934.64um:3958.43um] 23.79um 0--T--->
#  <id:6419904 Vertical core.subckt_12948_test_issuer.subckt_0_ti.subckt_3774_core.dec_duiuvu_duiuvu_insn(13) METAL3 [4242.48um 3958.68um] [4242.48um 3983.76um] 0.38um rpD:1 -----CG----W------bb- [3958.4um:3984.04um] 25.64um 0--T--->
#  TargetU:3958.43um SourceU:3958.4um
#DebugSession.addToTrace( katana.getCell().getNet( 'core.subckt_12948_test_issuer.subckt_0_ti.subckt_3774_core.subckt_4310_dec_duiuvu.dec_duiuvu_in2_sel_bit3_hfns_1' ) )
# M4 min area. U-Turn not supressed.
#DebugSession.addToTrace( katana.getCell().getNet( 'core.libresocsim_libresoc_interface0_sel_3_hfns_0' ) )
