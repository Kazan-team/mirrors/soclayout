
import os
import json
import sys
import traceback
import CRL
import helpers
from   helpers    import trace, overlay, l, u, n
from   helpers.io import ErrorMessage, WarningMessage
import plugins
from   Hurricane  import Breakpoint, DbU, DataBase, Instance, Transformation
from   plugins.alpha.block.configuration import IoPin, GaugeConf
from   plugins.alpha.block.iospecs       import IoSpecs
from   plugins.alpha.block.block         import Block
from   plugins.alpha.block.spares        import Spares
from   plugins.alpha.core2chip.niolib    import CoreToChip
from   plugins.alpha.chip.configuration  import ChipConf
from   plugins.alpha.chip.chip           import Chip


af  = CRL.AllianceFramework.get()

def scriptMain ( **kw ):
    """The mandatory function to be called by Coriolis CGT/Unicorn."""
    global af
    rvalue   = True
    coreSize = 380*100.0
    cwd      = os.path.split( os.path.abspath(__file__) )[0]
    ioSpecs  = IoSpecs()
    ioSpecs.loadFromPinmux( '%s/ls180/litex_pinpads.json' % cwd,
                            cheat_dont_do_analog=True)
    try:
        #helpers.setTraceLevel( 550 )
        #Breakpoint.setStopLevel( 100 )
        cell, editor = plugins.kwParseMain( **kw )
        cell = af.getCell( 'ls180', CRL.Catalog.State.Logical )
        if cell is None:
            print( ErrorMessage( 2, 'doDesign.scriptMain(): Unable to '
                                    'load cell "{}".'.format('ls180') ))
            sys.exit(1)
        if editor: editor.setCell( cell )
        ls180Conf = ChipConf( cell, ioPads=ioSpecs.ioPadsSpec )
        ls180Conf.cfg.etesian.bloat = 'nsxlib'
        ls180Conf.cfg.etesian.uniformDensity = True
        ls180Conf.cfg.etesian.aspectRatio = 1.0
        ls180Conf.cfg.etesian.spaceMargin = 0.05
        ls180Conf.cfg.katana.hTracksReservedLocal = 6
        ls180Conf.cfg.katana.vTracksReservedLocal = 6
        ls180Conf.cfg.katana.hTracksReservedMin = 0
        ls180Conf.cfg.katana.vTracksReservedMin = 0
        ls180Conf.cfg.block.spareSide = l(700)
        ls180Conf.cfg.chip.padCoreSide = 'North'
        ls180Conf.cfg.chip.supplyRailWidth = l(50)
        ls180Conf.cfg.chip.supplyRailPitch = l(300)
        ls180Conf.editor = editor
        ls180Conf.useSpares = True
        ls180Conf.useClockTree = True
        ls180Conf.useHFNS = True
        ls180Conf.bColumns = 2
        ls180Conf.bRows = 2
        ls180Conf.chipConf.name = 'chip'
        ls180Conf.chipConf.ioPadGauge = 'niolib'
        ls180Conf.coreSize = (l(coreSize      ), l(coreSize      ))
        ls180Conf.chipSize = (l(coreSize+4000.0), l(coreSize+4000.0))
        ls180Conf.useHTree( 'core.pll_clk', Spares.HEAVY_LEAF_LOAD )
        ls180Conf.useHTree('jtag_tck_from_pad')

        ls180ToChip = CoreToChip( ls180Conf )
        ls180ToChip.buildChip()

        chipBuilder = Chip( ls180Conf )
        chipBuilder.doChipFloorplan()

        tiPath    = 'test_issuer.ti.'
        sramDatas = [ 'test_issuer.ti.sram4k_0.spblock_512w64b8w'
                    , 'test_issuer.ti.sram4k_1.spblock_512w64b8w'
                    , 'test_issuer.ti.sram4k_2.spblock_512w64b8w'
                    , 'test_issuer.ti.sram4k_3.spblock_512w64b8w'
                    ]

        with overlay.UpdateSession():
            sram        = DataBase.getDB().getCell( 'spblock_512w64b8w' )
            sramAb      = sram.getAbutmentBox()
            coreAb      = cell.getAbutmentBox()
            sliceHeight = chipBuilder.conf.sliceHeight
            sliceStep   = chipBuilder.conf.sliceStep
            originX     = coreAb.getXMin() 
            for i in range(len(sramDatas)):
                block = chipBuilder.rgetCoreInstance( sramDatas[i] )
                block.setTransformation(  
                    Transformation( originX
                                  , coreAb.getYMax() - sramAb.getHeight()
                                  , Transformation.Orientation.ID ) )
                block.setPlacementStatus( Instance.PlacementStatus.FIXED )
                originX += sramAb.getWidth()
            block = chipBuilder.rgetCoreInstance( 'test_issuer.wrappll.pll' )
            block.setTransformation(  
                Transformation( coreAb.getXMax()
                              , coreAb.getYMax() - block.getAbutmentBox().getHeight()
                              , Transformation.Orientation.MX ) )
            block.setPlacementStatus( Instance.PlacementStatus.FIXED )

        rvalue = chipBuilder.doPnR()
        chipBuilder.save()
       #CRL.Gds.save(ls180Conf.chip)
    except Exception as e:
        helpers.io.catch(e)
        rvalue = False
    sys.stdout.flush()
    sys.stderr.flush()
    return rvalue
