
from   __future__ import print_function
import sys
import traceback
import CRL
import Cfg
import helpers
from   helpers.io      import ErrorMessage
from   helpers.io      import WarningMessage
from   helpers.overlay import CfgCache
from   helpers         import trace, l
import plugins
from   Hurricane  import DbU
from   Hurricane  import Pin
from   Hurricane  import Transformation
from   plugins.alpha.block.block         import Block
from   plugins.alpha.block.configuration import IoPin


af = CRL.AllianceFramework.get()


def coriolis_setup():
    with CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
        cfg.misc.catchCore = False
        cfg.misc.info = False
        cfg.misc.paranoid = False
        cfg.misc.bug = False
        cfg.misc.logMode = True
        cfg.misc.verboseLevel1 = True
        cfg.misc.verboseLevel2 = True
        cfg.etesian.effort = 2
        cfg.etesian.spaceMargin = 0.10  # 10%
        cfg.etesian.aspectRatio = 1.0   # 100%
        cfg.etesian.uniformDensity = True
        cfg.anabatic.edgeLenght = 24
        cfg.anabatic.edgeWidth = 8
        cfg.anabatic.topRoutingLayer = 'METAL5'
        cfg.katana.searchHalo = 30
        cfg.katana.eventsLimit = 1000000
        cfg.katana.hTracksReservedLocal = 7
        cfg.katana.vTracksReservedLocal = 6

        env = af.getEnvironment()
        env.setCLOCK('^clk$|m_clock')
        env.setPOWER('vdd')
        env.setGROUND('vss')


def scriptMain ( **kw ):
    """The mandatory function that Coriolis CGT/Unicorn will look for."""
    global af

    IW = IoPin.WEST
    IE = IoPin.EAST
    IS = IoPin.SOUTH
    IN = IoPin.NORTH
    AB = IoPin.A_BEGIN
    AE = IoPin.A_END

    alup=[
        (IW | AB, 'coresync_clk'                    , 0 ),
        (IW | AB, 'cu_issue_i'                      , 0 ),
        (IW | AB, 'oper_i_alu_alu0_imm_data_imm_ok' , 0 ),
        (IW | AB, 'oper_i_alu_alu0_invert_a'        , 0 ),
        (IW | AB, 'oper_i_alu_alu0_invert_out'      , 0 ),
        (IW | AB, 'oper_i_alu_alu0_is_32bit'        , 0 ),
        (IW | AB, 'oper_i_alu_alu0_is_signed'       , 0 ),
        (IW | AB, 'oper_i_alu_alu0_oe_oe'           , 0 ),
        (IW | AB, 'oper_i_alu_alu0_oe_oe_ok'        , 0 ),
        (IW | AB, 'oper_i_alu_alu0_output_carry'    , 0 ),
        (IW | AB, 'oper_i_alu_alu0_rc_rc'           , 0 ),
        (IW | AB, 'oper_i_alu_alu0_rc_rc_ok'        , 0 ),
        (IW | AB, 'oper_i_alu_alu0_write_cr0'       , 0 ),
        (IW | AB, 'oper_i_alu_alu0_zero_a'          , 0 ),
        (IW | AB, 'coresync_rst'                    , 0 ),
        (IW | AB, 'src3_i'                          , 0 ),
        (IW | AB, 'oper_i_alu_alu0_input_carry({})' , 0, l( 10.0),  2),
        (IW | AB, 'src4_i({})'                      , 0, l( 10.0),  2),
        (IW | AB, 'oper_i_alu_alu0_data_len({})'    , 0, l( 10.0),  4),
        (IW | AB, 'cu_rd_go_i({})'                  , 0, l( 10.0),  4),
        (IW | AB, 'cu_rdmaskn_i({})'                , 0, l( 10.0),  4),
        (IW | AB, 'cu_wr_go_i({})'                  , 0, l( 10.0),  5),
        (IW | AB, 'oper_i_alu_alu0_insn_type({})'   , 0, l( 10.0),  7),
        (IW | AB, 'oper_i_alu_alu0_fn_unit({})'     , 0, l( 10.0), 11),
        (IW | AB, 'oper_i_alu_alu0_insn({})'        , 0, l( 10.0), 32),
        (IW | AB, 'oper_i_alu_alu0_imm_data_imm({})', 0, l( 15.0), 64),
        (IS     , 'src1_i({})'                      , l(10), l( 15.0), 64),
        (IS     , 'src2_i({})'                      , l(15), l( 15.0), 64),
        (IN     , 'dest1_o({})'                     , l(20), l( 15.0), 64),
        (IE | AE, 'cu_busy_o'                       , 0 ),
        (IE | AE, 'cr_a_ok'                         , 0 ),
        (IE | AE, 'dest5_o'                         , 0 ),
        (IE | AE, 'o_ok'                            , 0 ),
        (IE | AE, 'xer_ca_ok'                       , 0 ),
        (IE | AE, 'xer_ov_ok'                       , 0 ),
        (IE | AE, 'xer_so_ok'                       , 0 ),
        (IE | AE, 'dest3_o({})'                     , 0, l( 20.0),  2),
        (IE | AE, 'dest4_o({})'                     , 0, l( 20.0),  2),
        (IE | AE, 'dest2_o({})'                     , 0, l( 20.0),  4),
        (IE | AE, 'cu_rd_rel_o({})'                 , 0, l( 20.0),  4),
        (IE | AE, 'cu_wr_rel_o({})'                 , 0, l( 20.0),  5),
        ]

    rvalue = True
    try:
        helpers.setTraceLevel( 550 )
        cell, editor = plugins.kwParseMain( **kw )

        alu0 = af.getCell( 'alu0', CRL.Catalog.State.Views )
        blockAlu0 = Block.create (alu0, ioPins = alup)
        blockAlu0.state.cfg.etesian.spaceMargin = 0.05
        blockAlu0.state.fixedHeight = l(5000)
        blockAlu0.state.useSpares   = False
       #rvalue = blockAlu0.build()
 
        mulp=[
            (IN     , 'coresync_clk'                    , l(4500.0) ),
            (IW | AB, 'cu_issue_i'                      , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_imm_data_imm_ok' , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_invert_a'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_invert_out'      , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_is_32bit'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_is_signed'       , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_oe_oe'           , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_oe_oe_ok'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_rc_rc'           , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_rc_rc_ok'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_write_cr0'       , 0, l(20) ),
            (IW | AB, 'oper_i_alu_mul0_zero_a'          , 0, l(20) ),
            (IW | AB, 'coresync_rst'                    , 0, l(20) ),
            (IW | AB, 'src3_i'                          , 0, l(20) ),
            (IW | AB, 'cu_rd_go_i({})'                  , 0, l(10.0),  3),
            (IW | AB, 'cu_rdmaskn_i({})'                , 0, l(10.0),  3),
            (IW | AB, 'cu_wr_go_i({})'                  , 0, l(10.0),  4),
            (IW | AB, 'oper_i_alu_mul0_insn_type({})'   , 0, l(10.0),  7),
            (IW | AB, 'oper_i_alu_mul0_fn_unit({})'     , 0, l(10.0), 11),
            (IW | AB, 'oper_i_alu_mul0_insn({})'        , 0, l(10.0), 32),
            (IW | AB, 'oper_i_alu_mul0_imm_data_imm({})', 0, l(15.0), 64),
            (IS | AB, 'src1_i({})'                      , l(10.0), l(50.0), 64),
            (IS | AB, 'src2_i({})'                      , l(20.0), l(50.0), 64),
            (IE | AE, 'cu_busy_o'                       , 0 ),
            (IE | AE, 'cr_a_ok'                         , 0 ),
            (IE | AE, 'dest4_o'                         , 0 ),
            (IE | AE, 'o_ok'                            , 0 ),
            (IE | AE, 'xer_ov_ok'                       , 0 ),
            (IE | AE, 'xer_so_ok'                       , 0 ),
            (IE | AE, 'dest3_o({})'                     , 0, l( 20.0),  2),
            (IE | AE, 'dest2_o({})'                     , 0, l( 20.0),  4),
            (IE | AE, 'cu_rd_rel_o({})'                 , 0, l( 20.0),  3),
            (IE | AE, 'cu_wr_rel_o({})'                 , 0, l( 20.0),  4),
            (IN | AE, 'dest1_o({})'                     , 0, l( 30.0), 64),
             ]
        mul0 = af.getCell( 'mul0', CRL.Catalog.State.Views )
        blockMul0 = Block.create ( mul0 , ioPins=mulp)
        blockMul0.state.cfg.etesian.uniformDensity = True
        blockMul0.state.cfg.etesian.spaceMargin    = 0.15
        blockMul0.state.cfg.katana.searchHalo      = 1
        blockMul0.state.fixedHeight = l(5000)
        blockMul0.state.useSpares   = False
       #rvalue = blockMul0.build()
 
        divp=[
            (IN     , 'coresync_clk'                    , l(4500.0) ),
            (IW | AB, 'cu_issue_i'                      , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_imm_data_imm_ok' , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_invert_a'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_invert_out'      , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_is_32bit'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_is_signed'       , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_oe_oe'           , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_oe_oe_ok'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_output_carry'    , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_rc_rc'           , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_rc_rc_ok'        , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_write_cr0'       , 0, l(20) ),
            (IW | AB, 'oper_i_alu_div0_zero_a'          , 0, l(20) ),
            (IW | AB, 'coresync_rst'                    , 0, l(20) ),
            (IW | AB, 'src3_i'                          , 0, l(20) ),
            (IW | AB, 'cu_rd_go_i({})'                  , 0, l(10.0),  3),
            (IW | AB, 'cu_rdmaskn_i({})'                , 0, l(10.0),  3),
            (IW | AB, 'cu_wr_go_i({})'                  , 0, l(10.0),  4),
            (IW | AB, 'oper_i_alu_div0_data_len({})'    , 0, l(10.0),  7),
            (IW | AB, 'oper_i_alu_div0_insn_type({})'   , 0, l(10.0),  7),
            (IW | AB, 'oper_i_alu_div0_fn_unit({})'     , 0, l(10.0), 11),
            (IW | AB, 'oper_i_alu_div0_insn({})'        , 0, l(10.0), 32),
            (IW | AB, 'oper_i_alu_div0_imm_data_imm({})', 0, l(15.0), 64),
            (IS | AB, 'src1_i({})'                      , l(10.0), l(50.0), 64),
            (IS | AB, 'src2_i({})'                      , l(20.0), l(50.0), 64),
            (IE | AE, 'cu_busy_o'                       , 0 ),
            (IE | AE, 'cr_a_ok'                         , 0 ),
            (IE | AE, 'dest4_o'                         , 0 ),
            (IE | AE, 'o_ok'                            , 0 ),
            (IE | AE, 'xer_ov_ok'                       , 0 ),
            (IE | AE, 'xer_so_ok'                       , 0 ),
            (IE | AE, 'dest3_o({})'                     , 0, l( 20.0),  2),
            (IE | AE, 'dest2_o({})'                     , 0, l( 20.0),  4),
            (IE | AE, 'cu_rd_rel_o({})'                 , 0, l( 20.0),  3),
            (IE | AE, 'cu_wr_rel_o({})'                 , 0, l( 20.0),  4),
            (IN | AE, 'dest1_o({})'                     , 0, l( 30.0), 64),
             ]
        div0 = af.getCell( 'div0', CRL.Catalog.State.Views )
        blockDiv0 = Block.create ( div0 , ioPins=divp)
        blockDiv0.state.cfg.etesian.uniformDensity = True
        blockDiv0.state.cfg.etesian.spaceMargin    = 0.10
        blockDiv0.state.cfg.katana.searchHalo      = 1
        blockDiv0.state.fixedHeight = l(2000)
        blockDiv0.state.useSpares   = False
       #rvalue = blockDiv0.build()
 
        branchp=[
          (IN, 'coresync_clk'                       , l( 805.0) ),
          (IW, 'cu_issue_i'                         , l(  30.0) ),
          (IW, 'oper_i_alu_branch0_imm_data_imm_ok' , l(  40.0) ),
          (IW, 'oper_i_alu_branch0_is_32bit'        , l(  70.0) ),
          (IW, 'oper_i_alu_branch0_lk'              , l( 150.0) ),
          (IW, 'coresync_rst'                       , l( 160.0) ),
          (IW, 'src3_i({})'                         , l( 180.0), l( 10.0),  4),
          (IW, 'cu_rd_go_i({})'                     , l( 270.0), l( 10.0),  3),
          (IW, 'cu_rdmaskn_i({})'                   , l( 310.0), l( 10.0),  3),
          (IW, 'cu_wr_go_i({})'                     , l( 350.0), l( 10.0),  3),
          (IW, 'oper_i_alu_branch0_insn_type({})'   , l( 400.0), l( 10.0),  7),
          (IW, 'oper_i_alu_branch0_fn_unit({})'     , l( 470.0), l( 10.0), 11),
          (IW, 'oper_i_alu_branch0_insn({})'        , l( 580.0), l( 10.0), 32),
          (IW, 'oper_i_alu_branch0_imm_data_imm({})', l( 900.0), l( 10.0), 64),
          (IW, 'oper_i_alu_branch0_cia({})'         , l(1540.0), l( 10.0), 64),
          (IS, 'src1_i({})'                         , l(  10.0), l( 20.0), 64),
          (IS, 'src2_i({})'                         , l(  15.0), l( 20.0), 64),
          (IE, 'cu_busy_o'                          , l(3500.0) ),
          (IE, 'fast1_ok'                           , l(3520.0) ),
          (IE, 'fast2_ok'                           , l(3540.0) ),
          (IE, 'nia_ok'                             , l(3560.0) ),
          (IE, 'dest2_o({})'                        , l(3580.0), l( 10.0), 64),
          (IE, 'dest3_o({})'                        , l(4220.0), l( 10.0), 64),
          (IE, 'cu_rd_rel_o({})'                    , l(4860.0), l( 20.0),  3),
          (IE, 'cu_wr_rel_o({})'                    , l(4920.0), l( 20.0),  3),
          (IN, 'dest1_o({})'                        , l( 500.0), l( 10.0), 64),
        ]
        branch0 = af.getCell( 'branch0', CRL.Catalog.State.Views )
        blockBranch0 = Block.create ( branch0 , ioPins=branchp)
        blockBranch0.state.cfg.etesian.spaceMargin = 0.07
        blockBranch0.state.fixedHeight = l(5000)
        blockBranch0.state.useSpares   = False
       #rvalue = blockBranch0.build()
 
        cr0 = af.getCell( 'cr0', CRL.Catalog.State.Views )
        blockCr0 = Block.create \
            ( cr0
            , ioPins=[
                (IN, 'coresync_clk'                       , l( 805.0) ),
                (IW, 'cu_issue_i'                         , l(  30.0) ),
                (IW, 'oper_i_alu_cr0_read_cr_whole'       , l(  40.0) ),
                (IW, 'oper_i_alu_cr0_write_cr_whole'      , l(  70.0) ),
                (IW, 'coresync_rst'                       , l( 160.0) ),
                (IW, 'src4_i({})'                         , l( 180.0), l( 10.0),  4),
                (IW, 'src5_i({})'                         , l( 220.0), l( 10.0),  4),
                (IW, 'src6_i({})'                         , l( 260.0), l( 10.0),  4),
                (IW, 'cu_rd_go_i({})'                     , l( 300.0), l( 10.0),  6),
                (IW, 'cu_rdmaskn_i({})'                   , l( 360.0), l( 10.0),  6),
                (IW, 'cu_wr_go_i({})'                     , l( 420.0), l( 10.0),  3),
                (IW, 'oper_i_alu_cr0_insn_type({})'       , l( 450.0), l( 10.0),  7),
                (IW, 'oper_i_alu_cr0_fn_unit({})'         , l( 520.0), l( 10.0), 11),
                (IW, 'oper_i_alu_cr0_insn({})'            , l( 630.0), l( 10.0), 32),
                (IS, 'src1_i({})'                         , l(  10.0), l( 10.0), 64),
                (IS, 'src2_i({})'                         , l(  15.0), l( 10.0), 64),
                (IE, 'src3_i({})'                         , l(  10.0), l( 20.0), 32),
                (IE, 'cu_busy_o'                          , l(4320.0) ),
                (IE, 'cr_a_ok'                            , l(4340.0) ),
                (IE, 'full_cr_ok'                         , l(4360.0) ),
                (IE, 'o_ok'                               , l(4380.0) ),
                (IE, 'dest2_o({})'                        , l(4400.0), l( 10.0), 32),
                (IE, 'dest3_o({})'                        , l(4720.0), l( 10.0),  4),
                (IE, 'cu_rd_rel_o({})'                    , l(4800.0), l( 20.0),  6),
                (IE, 'cu_wr_rel_o({})'                    , l(4920.0), l( 20.0),  3),
                (IN, 'dest1_o({})'                        , l( 100.0), l( 10.0), 64)
            ]
            )
        blockCr0.state.cfg.etesian.spaceMargin = 0.10
        blockCr0.state.fixedHeight = l(5000)
        blockCr0.state.useSpares   = False
       #rvalue = blockCr0.build()
 
        ldst0 = af.getCell( 'ldst0', CRL.Catalog.State.Views )
        blockLdst0 = Block.create \
            ( ldst0
            , ioPins=[
                (IN     , 'coresync_clk'                       , l(805.0) ),
                (IW | AB, 'cu_ad_go_i'                         , 0, l(20),  1),
                (IW | AB, 'cu_issue_i'                         , 0, l(20),  1),
                (IW | AB, 'ldst_port0_addr_exc_o'              , 0, l(20),  1),
                (IW | AB, 'ldst_port0_addr_ok_o'               , 0, l(20),  1),
                (IW | AB, 'ldst_port0_ld_data_o_ok'            , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_byte_reverse'     , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_imm_data_imm_ok'  , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_is_32bit'         , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_is_signed'        , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_oe_oe'            , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_oe_oe_ok'         , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_rc_rc'            , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_rc_rc_ok'         , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_sign_extend'      , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_zero_a'           , 0, l(20),  1),
                (IW | AB, 'coresync_rst'                       , 0, l(20),  1),
                (IW | AB, 'cu_st_go_i'                         , 0, l(20),  1),
                (IW | AB, 'oper_i_ldst_ldst0_ldst_mode({})'    , 0, l(20),  2),
                (IW | AB, 'cu_rd_go_i({})'                     , 0, l(20),  3),
                (IW | AB, 'cu_rdmaskn_i({})'                   , 0, l(20),  3),
                (IW | AB, 'cu_wr_go_i({})'                     , 0, l(20),  2),
                (IW | AB, 'oper_i_ldst_ldst0_data_len({})'     , 0, l(20),  4),
                (IW | AB, 'oper_i_ldst_ldst0_insn_type({})'    , 0, l(20),  7),
                (IW | AB, 'ldst_port0_ld_data_o({})'           , 0, l(20), 64),
                (IW | AB, 'oper_i_ldst_ldst0_imm_data_imm({})' , 0, l(20), 64),
                (IS | AB, 'src1_i({})'                         , 0,  l(5), 64),
                (IS | AB, 'src2_i({})'                         , 0,  l(5), 64),
                (IE | AE, 'src3_i({})'                         , 0,     0, 64),
                (IE | AE, 'cu_busy_o'                          , 0, l(20),  1),
                (IE | AE, 'cu_ad_rel_o'                        , 0, l(20),  1),
                (IE | AE, 'ldst_port0_addr_i_ok'               , 0, l(20),  1),
                (IE | AE, 'ldst_port0_is_ld_i'                 , 0, l(20),  1),
                (IE | AE, 'ldst_port0_is_st_i'                 , 0, l(20),  1),
                (IE | AE, 'load_mem_o'                         , 0, l(20),  1),
                (IE | AE, 'cu_st_rel_o'                        , 0, l(20),  1),
                (IE | AE, 'stwd_mem_o'                         , 0, l(20),  1),
                (IE | AE, 'ea({})'                             , 0, l(20), 64),
                (IE | AE, 'ldst_port0_st_data_i({})'           , 0, l(20), 64),
                (IE | AE, 'cu_rd_rel_o({})'                    , 0,    0,   3),
                (IE | AE, 'cu_wr_rel_o({})'                    , 0,    0,   2),
                (IE | AE, 'ldst_port0_addr_i_95'               , 0, l(20),  1),
                (IE | AE, 'ldst_port0_addr_i_{}'               , 0, l(20), 64),
                (IN | AE, 'o({})'                              , 0,     0, 64),
            ]
            )
        blockLdst0.state.cfg.etesian.uniformDensity      = True
        blockLdst0.state.cfg.etesian.spaceMargin         = 0.20
        blockLdst0.state.cfg.katana.searchHalo           = 1
        blockLdst0.state.cfg.katana.hTracksReservedLocal = 10
        blockLdst0.state.cfg.katana.vTracksReservedLocal = 10
        blockLdst0.state.fixedHeight = l(5000)
        blockLdst0.state.useSpares   = False
       #rvalue = blockLdst0.build()
 
        logical0 = af.getCell( 'logical0', CRL.Catalog.State.Views )
        blockLogical0 = Block.create \
            ( logical0
            , ioPins=[
                (IN     , 'coresync_clk'                        , l(805.0) ),
                (IW | AB, 'cu_issue_i'                          , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_imm_data_imm_ok' , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_invert_a'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_invert_out'      , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_is_32bit'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_is_signed'       , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_oe_oe'           , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_oe_oe_ok'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_output_carry'    , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_rc_rc'           , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_rc_rc_ok'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_write_cr0'       , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_zero_a'          , 0, l(20),  1),
                (IW | AB, 'coresync_rst'                        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_logical0_input_carry({})' , 0, l(20),  2),
                (IW | AB, 'cu_rd_go_i({})'                      , 0, l(20),  2),
                (IW | AB, 'cu_rdmaskn_i({})'                    , 0, l(20),  2),
                (IW | AB, 'cu_wr_go_i({})'                      , 0, l(20),  3),
                (IW | AB, 'oper_i_alu_logical0_data_len({})'    , 0, l(20),  4),
                (IW | AB, 'oper_i_alu_logical0_insn_type({})'   , 0, l(20),  7),
                (IW | AB, 'oper_i_alu_logical0_fn_unit({})'     , 0, l(20), 11),
                (IW | AB, 'oper_i_alu_logical0_insn({})'        , 0, l(20), 32),
                (IW | AB, 'oper_i_alu_logical0_imm_data_imm({})', 0, l(20), 64),
                (IS | AB, 'src1_i({})'                          , 0, l(10), 64),
                (IS | AB, 'src2_i({})'                          , 0,  l(5), 64),
                (IE | AE, 'cu_busy_o'                           , 0, l(20),  1),
                (IE | AE, 'cr_a_ok'                             , 0, l(20),  1),
                (IE | AE, 'o_ok'                                , 0, l(20),  1),
                (IE | AE, 'xer_ca_ok'                           , 0, l(20),  1),
                (IE | AE, 'cu_rd_rel_o({})'                     , 0,     0,  2),
                (IE | AE, 'cu_wr_rel_o({})'                     , 0,     0,  3),
                (IN | AE, 'dest3_o({})'                         , 0,     0,  2),
                (IN | AE, 'dest2_o({})'                         , 0,     0,  4),
                (IN | AE, 'dest1_o({})'                         , 0,     0, 64),
            ]
            )
        blockLogical0.state.cfg.etesian.uniformDensity = True
        blockLogical0.state.cfg.etesian.spaceMargin    = 0.07
        blockLogical0.state.cfg.katana.searchHalo      = 1
        blockLogical0.state.fixedHeight = l(5000)
        blockLogical0.state.useSpares   = False
       #rvalue = blockLogical0.build()
 
        shiftrot0 = af.getCell( 'shiftrot0', CRL.Catalog.State.Views )
        blockShiftrot0 = Block.create \
            ( shiftrot0
            , ioPins=[
                (IN     , 'coresync_clk'                          , l(805.0) ),
                (IW | AB, 'cu_issue_i'                            , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_imm_data_imm_ok' , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_input_cr'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_is_32bit'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_is_signed'       , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_oe_oe'           , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_oe_oe_ok'        , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_output_carry'    , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_output_cr'       , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_rc_rc'           , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_rc_rc_ok'        , 0, l(20),  1),
                (IW | AB, 'coresync_rst'                          , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_shift_rot0_input_carry({})' , 0, l(20),  2),
                (IW | AB, 'src4_i({})'                            , 0, l(10),  2),
                (IW | AB, 'cu_rd_go_i({})'                        , 0, l(20),  4),
                (IW | AB, 'cu_rdmaskn_i({})'                      , 0, l(20),  4),
                (IW | AB, 'cu_wr_go_i({})'                        , 0, l(20),  3),
                (IW | AB, 'oper_i_alu_shift_rot0_insn_type({})'   , 0, l(20),  7),
                (IW | AB, 'oper_i_alu_shift_rot0_fn_unit({})'     , 0, l(20), 11),
                (IW | AB, 'oper_i_alu_shift_rot0_insn({})'        , 0, l(20), 32),
                (IW | AB, 'oper_i_alu_shift_rot0_imm_data_imm({})', 0, l(20), 64),
                (IW | AB, 'src3_i({})'                            , 0, l(10), 64),
                (IS | AB, 'src1_i({})'                            , 0, l(10), 64),
                (IS | AB, 'src2_i({})'                            , 0,  l(5), 64),
                (IE | AE, 'cu_busy_o'                             , 0, l(20),  1),
                (IE | AE, 'cr_a_ok'                               , 0, l(20),  1),
                (IE | AE, 'o_ok'                                  , 0, l(20),  1),
                (IE | AE, 'xer_ca_ok'                             , 0, l(20),  1),
                (IE | AE, 'cu_rd_rel_o({})'                       , 0,     0,  4),
                (IE | AE, 'cu_wr_rel_o({})'                       , 0,     0,  3),
                (IN | AE, 'dest3_o({})'                           , 0,     0,  2),
                (IN | AE, 'dest2_o({})'                           , 0,     0,  4),
                (IN | AE, 'dest1_o({})'                           , 0,     0, 64),
            ]
            )
        blockShiftrot0.state.cfg.etesian.uniformDensity = True
        blockShiftrot0.state.cfg.etesian.spaceMargin    = 0.7
        blockShiftrot0.state.cfg.katana.searchHalo      = 1
        blockShiftrot0.state.fixedHeight = l(5000)
        blockShiftrot0.state.useSpares   = False
       #rvalue = blockShiftrot0.build()

        spr0 = af.getCell( 'spr0', CRL.Catalog.State.Views )
        blockSpr0 = Block.create \
            ( spr0
            , ioPins=[
                (IN     , 'coresync_clk'                 , l(805.0) ),
                (IW | AB, 'cu_issue_i'                   , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_spr0_is_32bit'     , 0, l(20),  1),
                (IW | AB, 'coresync_rst'                 , 0, l(20),  1),
                (IW | AB, 'src4_i'                       , 0, l(10),  1),
                (IW | AB, 'src5_i({})'                   , 0, l(10),  2),
                (IW | AB, 'src6_i({})'                   , 0, l(10),  2),
                (IW | AB, 'cu_rd_go_i({})'               , 0, l(20),  6),
                (IW | AB, 'cu_rdmaskn_i({})'             , 0, l(20),  6),
                (IW | AB, 'cu_wr_go_i({})'               , 0, l(20),  6),
                (IW | AB, 'oper_i_alu_spr0_insn_type({})', 0, l(20),  7),
                (IW | AB, 'oper_i_alu_spr0_fn_unit({})'  , 0, l(20), 11),
                (IW | AB, 'oper_i_alu_spr0_insn({})'     , 0, l(20), 32),
                (IW | AB, 'src3_i({})'                   , 0, l(10), 64),
                (IS | AB, 'src1_i({})'                   , 0, l(10), 64),
                (IS | AB, 'src2_i({})'                   , 0,  l(5), 64),
                (IE | AE, 'cu_busy_o'                    , 0, l(20),  1),
                (IE | AE, 'dest4_o'                      , 0, l(20),  1),
                (IE | AE, 'fast1_ok'                     , 0, l(20),  1),
                (IE | AE, 'o_ok'                         , 0, l(20),  1),
                (IE | AE, 'spr1_ok'                      , 0, l(20),  1),
                (IE | AE, 'xer_ca_ok'                    , 0, l(20),  1),
                (IE | AE, 'xer_ov_ok'                    , 0, l(20),  1),
                (IE | AE, 'xer_so_ok'                    , 0, l(20),  1),
                (IE | AE, 'cu_rd_rel_o({})'              , 0,     0,  6),
                (IE | AE, 'cu_wr_rel_o({})'              , 0,     0,  6),
                (IE | AE, 'dest5_o({})'                  , 0,     0,  2),
                (IE | AE, 'dest6_o({})'                  , 0,     0,  2),
                (IE | AE, 'dest3_o({})'                  , 0, l(20), 64),
                (IS | AE, 'dest2_o({})'                  , 0, l(20), 64),
                (IS | AE, 'dest1_o({})'                  , 0, l(20), 64),
            ]
            )
        blockSpr0.state.cfg.etesian.uniformDensity = True
        blockSpr0.state.cfg.etesian.spaceMargin    = 0.5
        blockSpr0.state.cfg.katana.searchHalo      = 1
        blockSpr0.state.fixedHeight = l(2200)
        blockSpr0.state.useSpares   = False
       #rvalue = blockSpr0.build()

        trap0 = af.getCell( 'trap0', CRL.Catalog.State.Views )
        blockTrap0 = Block.create \
            ( trap0
            , ioPins=[
                (IN     , 'coresync_clk'                  , l(805.0) ),
                (IW | AB, 'cu_issue_i'                    , 0, l(20),  1),
                (IW | AB, 'oper_i_alu_trap0_is_32bit'     , 0, l(20),  1),
                (IW | AB, 'coresync_rst'                  , 0, l(20),  1),
                (IW | AB, 'cu_rd_go_i({})'                , 0, l(20),  4),
                (IW | AB, 'cu_rdmaskn_i({})'              , 0, l(20),  4),
                (IW | AB, 'cu_wr_go_i({})'                , 0, l(20),  5),
                (IW | AB, 'oper_i_alu_trap0_traptype({})' , 0, l(20),  5),
                (IW | AB, 'oper_i_alu_trap0_insn_type({})', 0, l(20),  7),
                (IW | AB, 'oper_i_alu_trap0_fn_unit({})'  , 0, l(20), 11),
                (IW | AB, 'oper_i_alu_trap0_trapaddr({})' , 0, l(20), 13),
                (IW | AB, 'oper_i_alu_trap0_insn({})'     , 0, l(20), 32),
                (IW | AB, 'oper_i_alu_trap0_cia({})'      , 0, l(20), 64),
                (IW | AB, 'oper_i_alu_trap0_msr({})'      , 0, l(20), 64),
                (IW | AB, 'src3_i({})'                    , 0, l(10), 64),
                (IS | AB, 'src4_i({})'                    , 0, l(10), 64),
                (IS | AB, 'src1_i({})'                    , 0, l(10), 64),
                (IS | AB, 'src2_i({})'                    , 0,  l(5), 64),
                (IE | AE, 'cu_busy_o'                     , 0, l(20),  1),
                (IE | AE, 'fast1_ok'                      , 0, l(20),  1),
                (IE | AE, 'fast2_ok'                      , 0, l(20),  1),
                (IE | AE, 'msr_ok'                        , 0, l(20),  1),
                (IE | AE, 'nia_ok'                        , 0, l(20),  1),
                (IE | AE, 'o_ok'                          , 0, l(20),  1),
                (IE | AE, 'cu_rd_rel_o({})'               , 0,     0,  4),
                (IE | AE, 'cu_wr_rel_o({})'               , 0,     0,  5),
                (IN | AE, 'dest5_o({})'                   , 0, l(10), 64),
                (IN | AE, 'dest4_o({})'                   , 0, l(10), 64),
                (IE | AE, 'dest3_o({})'                   , 0, l(10), 64),
                (IE | AE, 'dest2_o({})'                   , 0, l(10), 64),
                (IE | AE, 'dest1_o({})'                   , 0, l(10), 64),
            ]
            )
        blockTrap0.state.cfg.etesian.uniformDensity = True
        blockTrap0.state.cfg.etesian.spaceMargin    = 0.5
        blockTrap0.state.cfg.katana.searchHalo      = 1
        blockTrap0.state.fixedHeight = l(5000)
        blockTrap0.state.useSpares   = False
       #rvalue = blockTrap0.build()
 
        fast = af.getCell( 'fast', CRL.Catalog.State.Views )
        blockFast = Block.create \
            ( fast
            , ioPins=[
                (IN     , 'coresync_clk'    , l(805.0) ),
                (IW | AB, 'coresync_rst'    , 0, l(20),  1),
                (IW | AB, 'cia_ren({})'     , 0, l(20),  8),
                (IW | AB, 'fast_nia_wen({})', 0, l(20),  8),
                (IW | AB, 'msr_ren({})'     , 0, l(20),  8),
                (IW | AB, 'src1_ren({})'    , 0, l(20),  8),
                (IW | AB, 'src2_ren({})'    , 0, l(20),  8),
                (IW | AB, 'wen({})'         , 0, l(20),  8),
                (IW | AB, 'wen_1({})'       , 0, l(20),  8),
                (IW | AB, 'wen_3({})'       , 0, l(20),  8),
                (IW | AB, 'wen_6({})'       , 0, l(20),  8),
                (IS | AB, 'data_i({})'      , 0, l(20), 64),
                (IS | AB, 'data_i_2({})'    , 0, l(20), 64),
                (IS | AB, 'data_i_4({})'    , 0, l(10), 64),
                (IS | AB, 'data_i_5({})'    , 0, l(10), 64),
                (IS | AB, 'data_i_7({})'    , 0, l(10), 64),
                (IN | AE, 'cia_data_o({})'  , 0, l(20), 64),
                (IN | AE, 'msr_data_o({})'  , 0, l(10), 64),
                (IN | AE, 'src1_data_o({})' , 0, l(10), 64),
                (IN | AE, 'src2_data_o({})' , 0, l(10), 64),
            ]
            )
        blockFast.state.cfg.etesian.uniformDensity = True
        blockFast.state.cfg.etesian.aspectRatio    = 1.0
        blockFast.state.cfg.etesian.spaceMargin    = 0.6
        blockFast.state.cfg.katana.searchHalo      = 1
        blockFast.state.useSpares   = False
       #rvalue = blockFast.build()
 
        cellInt  = af.getCell( 'int', CRL.Catalog.State.Views )
        blockInt = Block.create \
            ( cellInt
            , ioPins=[
                (IN     , 'coresync_clk'    , l(805.0) ),
                (IW | AB, 'coresync_rst'    , 0, l(20),  1),
                (IW | AB, 'wen({})'         , 0, l(20), 32),
                (IW | AB, 'wen_1({})'       , 0, l(20), 32),
                (IN | AB, 'dmi_ren({})'     , 0, l(20), 32),
                (IW | AB, 'src1_ren({})'    , 0, l(20), 32),
                (IW | AB, 'src2_ren({})'    , 0, l(20), 32),
                (IW | AB, 'src3_ren({})'    , 0, l(20), 32),
                (IS | AB, 'data_i({})'      , 0, l(20), 64),
                (IS | AB, 'data_i_2({})'    , 0, l(20), 64),
                (IN | AE, 'dmi_data_o({})'  , 0, l(10), 64),
                (IN | AE, 'src1_data_o({})' , 0, l(10), 64),
                (IN | AE, 'src2_data_o({})' , 0, l(10), 64),
                (IN | AE, 'src3_data_o({})' , 0, l(10), 64),
            ]
            )
        blockInt.state.cfg.etesian.uniformDensity = True
        blockInt.state.cfg.etesian.aspectRatio    = 1.0
        blockInt.state.cfg.etesian.spaceMargin    = 0.07
        blockInt.state.cfg.katana.searchHalo      = 1
        blockInt.state.useSpares   = False
       #rvalue = blockInt.build()
 
        issuer      = af.getCell( 'ls180' , CRL.Catalog.State.Logical )
        blockIssuer = Block.create \
            ( issuer
            , ioPins=[]
            )
 
        # Cell width:
        #
        # ================  =================
        # alu0              1200
        # branch0           1750
        # cr0                950
        # ldst0             1100
        # logical0          1800
        # mul0              9600
        # shiftrot0         2350
        # spr0              1550
        # trap0             3250
        # fast              ?
        # int               ?
        # pdecode           ?
        # ================  =================
 
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_0_alu0',
                    Transformation( l(1000), l(4000),
                    Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_1_branch0',
                    Transformation( l(2700), l(4000),
                    Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_2_cr0' ,
                    Transformation( l(4950), l(4000),
                    Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_3_div0' ,
                    Transformation( l(27000), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_4_ldst0' ,
                    Transformation( l(6400), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_5_logical0' ,
                    Transformation( l(8000), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_6_mul0' ,
                    Transformation( l(10300), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_7_shiftrot0' ,
                    Transformation( l(20400), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_8_spr0' ,
                    Transformation( l(23250), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2227_fus.subckt_9_trap0' ,
                    Transformation( l(25300), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2226_fast' ,
                    Transformation( l(1000), l(4000),
                        Transformation.Orientation.ID ))
        blockIssuer.useBlockInstance(
                    'subckt_1150_core.subckt_2228_int' ,
                    Transformation( l(1000), l(4000),
                        Transformation.Orientation.ID ))
        # TODO, create pdecode2 block
        #blockIssuer.useBlockInstance(
        #            'subckt_1150_core.subckt_2230_pdecode2' ,
        #            Transformation( l(1000), l(4000),
        #                Transformation.Orientation.ID ))
 
        blockIssuer.state.cfg.etesian.uniformDensity = True
        blockIssuer.state.cfg.etesian.aspectRatio    = 1.0
        blockIssuer.state.cfg.etesian.spaceMargin    = 0.07
        blockIssuer.state.cfg.katana.searchHalo      = 10000
        blockIssuer.state.fixedHeight = l(15000)
        blockIssuer.state.fixedWidth  = l(31550)
        blockIssuer.state.useSpares   = False
        blockIssuer.state.editor      = editor
        rvalue = blockIssuer.build()
    except Exception, e:
        helpers.io.catch( e )
        rvalue = False

    sys.stdout.flush()
    sys.stderr.flush()

    return rvalue


if __name__ == '__main__':
    coriolis_setup()
    kwargs = {}
    success = scriptMain(**kwargs)
    shellSuccess = 0
    if not success:
        shellSuccess = 1

    sys.exit(shellSuccess)
