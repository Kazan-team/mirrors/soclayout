#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import traceback
import os.path
import optparse
import Cfg
import Hurricane
from   Hurricane import DbU
from   Hurricane import DataBase
from   Hurricane import UpdateSession
from   Hurricane import Breakpoint
from   Hurricane import Box
from   Hurricane import Transformation
from   Hurricane import Instance
from   Hurricane import Contact
from   Hurricane import Vertical
from   Hurricane import Horizontal
from   Hurricane import Pin
from   Hurricane import NetExternalComponents
import Viewer
import CRL
import Etesian
import Anabatic
import Katana
import Unicorn
from   helpers   import l, u, n
import clocktree.ClockTree
import plugins.RSavePlugin
import plugins.ClockTreePlugin
import symbolic.cmos

af = CRL.AllianceFramework.get()


def toDbU ( l ): return DbU.fromLambda(l)

def createVertical ( contacts, x, layer, width=None):
    def yincrease ( lhs, rhs ): return int(lhs.getY() - rhs.getY())

    contacts.sort( yincrease )

    if width is None: width = l(2.0)

    for i in range(1,len(contacts)):
        print "create vert", contacts[i-1], contacts[i], layer, x, width
        v = Vertical.create( contacts[i-1], contacts[i], layer, x, width )
        print "v", v


def createHorizontal ( contactPaths, y, layer, width=None):
    def xincrease ( lhs, rhs ): return int(lhs.getX() - rhs.getX())

    contacts = contactPaths

    if width is None: width = l(2.0)

    contacts.sort( xincrease )

    for i in range(1,len(contacts)):
        Horizontal.create( contacts[i-1], contacts[i], layer, y, width )


def build_crosstrace(net, via, layer, x, x1, y):

    contacts = \
      [ Contact.create( net, via, l(x), l(y), l(1.0), l(1.0) )
      , Contact.create( net, via, l(x1), l(y), l(1.0), l(1.0) )
      ]

    createHorizontal( contacts, l(y), layer )
    print "slaves", contacts[-1].getSlaveComponents()
    for component in contacts[-1].getSlaveComponents():
        NetExternalComponents.setExternal(component)

def build_downtrace(net, via, layer, x, y, y1):

    contacts = \
      [ Contact.create( net, via, l(x), l(y), l(1.0), l(1.0) )
      , Contact.create( net, via, l(x), l(y1), l(1.0), l(1.0) )
      ]

    createVertical( contacts, l(x), layer )
    print "slaves", contacts[-1].getSlaveComponents()
    for component in contacts[-1].getSlaveComponents():
        NetExternalComponents.setExternal(component)

#print "af", dir(af)
#sys.exit(0)

Cfg.Configuration.pushDefaultPriority( Cfg.Parameter.Priority.UserFile )

cellsTop = '~/alliance-check-toolkit/cells'
env = af.getEnvironment()
env.addSYSTEM_LIBRARY( library=cellsTop+'/nsxlib', mode=CRL.Environment.Prepend )
env.addSYSTEM_LIBRARY( library=cellsTop+'/mpxlib', mode=CRL.Environment.Prepend )


Cfg.getParamBool      ( 'misc.catchCore'              ).setBool      ( False   )
Cfg.getParamBool      ( 'misc.info'                   ).setBool      ( False   )
Cfg.getParamBool      ( 'misc.paranoid'               ).setBool      ( False   )
Cfg.getParamBool      ( 'misc.bug'                    ).setBool      ( False   )
Cfg.getParamBool      ( 'misc.logMode'                ).setBool      ( True   )
Cfg.getParamBool      ( 'misc.verboseLevel1'          ).setBool      ( True    )
Cfg.getParamBool      ( 'misc.verboseLevel2'          ).setBool      ( True    )
#Cfg.getParamInt       ( 'misc.minTraceLevel'          ).setInt       ( 159     )
#Cfg.getParamInt       ( 'misc.maxTraceLevel'          ).setInt       ( 160     )
Cfg.getParamEnumerate ( 'etesian.effort'              ).setInt       ( 2       )
Cfg.getParamPercentage( 'etesian.spaceMargin'         ).setPercentage( 20.0    )
Cfg.getParamPercentage( 'etesian.aspectRatio'         ).setPercentage( 100.0   )
Cfg.getParamBool      ( 'etesian.uniformDensity'      ).setBool      ( True    )
Cfg.getParamInt       ( 'anabatic.edgeLenght'         ).setInt       ( 24      )
Cfg.getParamInt       ( 'anabatic.edgeWidth'          ).setInt       ( 8       )
Cfg.getParamString    ( 'anabatic.topRoutingLayer'    ).setString    ( 'METAL5')
Cfg.getParamInt       ( 'katana.eventsLimit'          ).setInt       ( 1000000 )
Cfg.getParamInt       ( 'katana.hTracksReservedLocal' ).setInt       ( 7       )
Cfg.getParamInt       ( 'katana.vTracksReservedLocal' ).setInt       ( 6       )
#Cfg.getParamInt       ( 'clockTree.minimumSide'       ).setInt       ( l(1000) )

env = af.getEnvironment()
env.setCLOCK( '^clk$|m_clock' )
env.setPOWER( 'vdd' )
env.setGROUND( 'vss' )

Cfg.Configuration.popDefaultPriority()

###################
# add

def add ( **kw ):
  editor = None
  if kw.has_key('editor') and kw['editor']:
    editor = kw['editor']

  db = DataBase.getDB()
  print db, dir(db)
  metal2 = DataBase.getDB().getTechnology().getLayer( 'metal2' )
  metal3 = DataBase.getDB().getTechnology().getLayer( 'metal3' )
  metal5 = DataBase.getDB().getTechnology().getLayer( 'metal5' )

  cell = af.getCell( 'add', CRL.Catalog.State.Logical )
  print cell.getNet('a(0)')

  if not cell:
    print '[ERROR] Unable to load cell "alu16.vst", aborting .'
    return False
  kw[ 'cell' ] = cell

  width = 350.0
  height = 405.0

  ab = Box( l(    0.0 )
          , l(    0.0 )
          , l( width )
          , l( height ) )

  cellGauge   = af.getCellGauge()
  spaceMargin = (Cfg.getParamPercentage('etesian.spaceMargin').asPercentage()+5) / 100.0
  aspectRatio =  Cfg.getParamPercentage('etesian.aspectRatio').asPercentage()    / 100.0
  clocktree.ClockTree.computeAbutmentBox( cell, spaceMargin, aspectRatio, cellGauge )
  ab2 = cell.getAbutmentBox()
  print "box", ab, ab.getHeight(), ab.getWidth()
  print "calc box", ab2, ab2.getHeight(), ab2.getWidth()

  #height = ab.getHeight()
  #width = ab.getWidth()

  UpdateSession.open()
  cell.setAbutmentBox( ab )

  for i in range(16):
    if True:
        x = 20.0*i + 10.0
        y = height
        Pin.create( cell.getNet('a(%d)' % i)
              , 'a(%d).0' % i
              , Pin.Direction.NORTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( x ), l( y - 0 )   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )
  for i in range(16):
    if True:
        Pin.create( cell.getNet('o(%d)' % i)
              , 'o(%d).0' % i
              , Pin.Direction.SOUTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( 10.0*i + 100.0 ), l( 0)   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )

  for i in range(16):
    if True:
        net = cell.getNet('b(%d)' % i)
        x = 20.0*i + 10.0 + 10
        y = height - 0
        #build_downtrace(net, metal3, x, y+11, y)
        #continue
        Pin.create( net
              , 'b(%d).0' % i
              , Pin.Direction.NORTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( x ), l( y - 0 )   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )
  if False:
    Pin.create( cell.getNet('rst')
            , 'p_reset.0'
            , Pin.Direction.WEST
            , Pin.PlacementStatus.FIXED
            , metal2
            , l(   0.0 )
            , l( 140.0 )
            , l(   2.0 )
            , l(   2.0 )
            )
  UpdateSession.close()

  if True:
        if editor: editor.setCell( cell )

        etesian = Etesian.EtesianEngine.create(cell)
        etesian.place()

        katana = Katana.KatanaEngine.create(cell)
        katana.digitalInit          ()
        #katana.runNegociatePreRouted()
        print dir(katana)
        katana.runGlobalRouter      (0)
        katana.loadGlobalRouting    ( Anabatic.EngineLoadGrByNet )
        katana.layerAssign          ( Anabatic.EngineNoNetLayerAssign )
        katana.runNegociate         ( Katana.Flags.NoFlags )
        katana.finalizeLayout       ()
        print dir(katana)
        success = katana.getSuccessState()
        katana.destroy()

  if True:
    VIA23 = DataBase.getDB().getTechnology().getLayer( 'VIA23' )
    UpdateSession.open()
    #net = cell.getNet('b(%d)' % 0)
    net = cell.getNet('vdd')
    build_downtrace(net, VIA23, metal2, -5, -10, -20)
    build_downtrace(net, VIA23, metal2, -10, -10, -20)
    build_crosstrace(net, VIA23, metal2, -5, -10, -10)
    build_crosstrace(net, VIA23, metal2, -5, -10, -20)
    for i in range(16):
        if False:
            net = cell.getNet('b(%d)' % i)
            x = 20.0*i + 10.0 + 10
            y = height-10
            build_downtrace(net, metal2, x, y, y+10)
    ab.inflate ( l(30.0) )
    cell.setAbutmentBox( ab )
    UpdateSession.close()

  #af.saveCell( cell, CRL.Catalog.State.Views )
  plugins.RSavePlugin.ScriptMain( **kw )

###################
# sub

def sub ( **kw ):
  editor = None
  if kw.has_key('editor') and kw['editor']:
    editor = kw['editor']

  db = DataBase.getDB()
  print db, dir(db)
  metal2 = DataBase.getDB().getTechnology().getLayer( 'metal2' )
  metal3 = DataBase.getDB().getTechnology().getLayer( 'metal3' )
  metal5 = DataBase.getDB().getTechnology().getLayer( 'metal5' )

  cell = af.getCell( 'sub', CRL.Catalog.State.Logical )
  print cell.getNet('a(0)')

  if not cell:
    print '[ERROR] Unable to load cell "alu16.vst", aborting .'
    return False
  kw[ 'cell' ] = cell

  width = 350.0
  height = 405.0

  ab = Box( l(    0.0 )
          , l(    0.0 )
          , l( width )
          , l( height ) )

  cellGauge   = af.getCellGauge()
  spaceMargin = (Cfg.getParamPercentage('etesian.spaceMargin').asPercentage()+5) / 100.0
  aspectRatio =  Cfg.getParamPercentage('etesian.aspectRatio').asPercentage()    / 100.0
  clocktree.ClockTree.computeAbutmentBox( cell, spaceMargin, aspectRatio, cellGauge )
  ab2 = cell.getAbutmentBox()
  print "box", ab, ab.getHeight(), ab.getWidth()
  print "calc box", ab2, ab2.getHeight(), ab2.getWidth()

  #height = ab.getHeight()
  #width = ab.getWidth()

  UpdateSession.open()
  cell.setAbutmentBox( ab )

  for i in range(16):
    if True:
        x = 20.0*i + 10.0
        y = height
        Pin.create( cell.getNet('a(%d)' % i)
              , 'a(%d).0' % i
              , Pin.Direction.NORTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( x ), l( y - 0 )   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )
  for i in range(16):
    if True:
        Pin.create( cell.getNet('o(%d)' % i)
              , 'o(%d).0' % i
              , Pin.Direction.SOUTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( 10.0*i + 100.0 ), l( 0)   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )

  for i in range(16):
    if True:
        net = cell.getNet('b(%d)' % i)
        x = 20.0*i + 10.0 + 10
        y = height - 0
        #build_downtrace(net, metal3, x, y+11, y)
        #continue
        Pin.create( net
              , 'b(%d).0' % i
              , Pin.Direction.NORTH
              , Pin.PlacementStatus.FIXED
              , metal3
              , l( x ), l( y - 0 )   # Position.
              , l( 2.0 )            , l( 2.0 )  # Size.
              )
  if False:
    Pin.create( cell.getNet('rst')
            , 'p_reset.0'
            , Pin.Direction.WEST
            , Pin.PlacementStatus.FIXED
            , metal2
            , l(   0.0 )
            , l( 140.0 )
            , l(   2.0 )
            , l(   2.0 )
            )
  UpdateSession.close()

  if True:
        if editor: editor.setCell( cell )

        etesian = Etesian.EtesianEngine.create(cell)
        etesian.place()

        katana = Katana.KatanaEngine.create(cell)
        katana.digitalInit          ()
        #katana.runNegociatePreRouted()
        print dir(katana)
        katana.runGlobalRouter      (0)
        katana.loadGlobalRouting    ( Anabatic.EngineLoadGrByNet )
        katana.layerAssign          ( Anabatic.EngineNoNetLayerAssign )
        katana.runNegociate         ( Katana.Flags.NoFlags )
        katana.finalizeLayout       ()
        print dir(katana)
        success = katana.getSuccessState()
        katana.destroy()

  if False:
    UpdateSession.open()
    cell.setAbutmentBox( ab )
    for i in range(16):
        if True:
            net = cell.getNet('b(%d)' % i)
            x = 20.0*i + 10.0 + 10
            y = height-10
            build_downtrace(net, metal2, x, y, y+10)
    UpdateSession.close()

  #af.saveCell( cell, CRL.Catalog.State.Views )
  plugins.RSavePlugin.ScriptMain( **kw )

def connect ( instanceRef, pin, netRef ):
    instance = instanceRef

    if isinstance(netRef,str):
      net = self.getNet( netRef )
    else:
      net = netRef

    masterNet = instance.getMasterCell().getNet(pin)
    if not masterNet:
      print '[ERR] Master cell "%s" of instance "%s" no connector named "%s".' \
          % (instance.getMasterCell().getName(), instance.getName(), pin)

    instance.getPlug( instance.getMasterCell().getNet(pin) ).setNet( net )

def place ( instanceRef, x, y, orient ):
    print "place", instanceRef, x, y, orient
    instance = instanceRef
    instance.setTransformation ( Transformation( x, y, orient ) )
    instance.setPlacementStatus( Instance.PlacementStatus.PLACED )

def createInstance ( cell, instanceName, modelRef, portmap={}, transf=None ):
    instance = cell.getInstance( instanceName )
    if instance:
        print "found instace", instance
        return instance
    if isinstance(modelRef,str):
        model = af.getCell( modelRef, Catalog.State.Views )
    else:
        model = modelRef
    instance = Instance.create( self.cell, instanceName, model )
    for pin, net in portmap.items():
        connect( instance, pin, net )

    print "create transf", transf
    if transf:
        place( instance, transf[0], transf[1], transf[2] )
    return instance

################# SNX
#

def alu_hier_place(**kw):

  editor = None
  if kw.has_key('editor') and kw['editor']:
    editor = kw['editor']

  db = DataBase.getDB()
  print db, dir(db)
  metal2 = DataBase.getDB().getTechnology().getLayer( 'metal2' )
  metal3 = DataBase.getDB().getTechnology().getLayer( 'metal3' )

  add = af.getCell( 'add', CRL.Catalog.State.Views )
  sub = af.getCell( 'sub', CRL.Catalog.State.Views )
  cell = af.getCell( 'alu_hier', CRL.Catalog.State.Logical )
  if not cell:
    print '[ERROR] Unable to load cell "snx.vst", aborting .'
    return False
  kw[ 'cell' ] = cell

  ab = Box( l(    0.0 )
          , l(    0.0 )
          , l( 1000.0 )
          , l(  600.0 ) )

  UpdateSession.open()
  cell.setAbutmentBox( ab )

  transf = (l(50.0), l(100.0), Transformation.Orientation.ID)
  subi = createInstance(cell, "subckt_49_sub", sub)
  place( subi, transf[0], transf[1], transf[2] )
  print "sub place", subi

  transf = (l(550.0), l(100.0), Transformation.Orientation.ID)
  addi = createInstance(cell, "subckt_48_add", add)
  place( addi, transf[0], transf[1], transf[2] )
  print "add place", addi

  if False:
      for i in range(16):
        Pin.create( cell.getNet('a(%d)' % i)
                  , 'a(%d).0' % i
                  , Pin.Direction.SOUTH
                  , Pin.PlacementStatus.FIXED
                  , metal3
                  , l( 100.0*i + 50.0 ) , l( 0.0 )  # Position.
                  , l( 2.0 )            , l( 2.0 )  # Size.
                  )
        Pin.create( cell.getNet('b(%d)' % i)
                  , 'b(%d).0' % i
                  , Pin.Direction.SOUTH
                  , Pin.PlacementStatus.FIXED
                  , metal3
                  , l( 100.0*i + 50.0 ) , l( 0.0 )  # Position.
                  , l( 2.0 )            , l( 2.0 )  # Size.
                  )
        Pin.create( cell.getNet('o(%d)' % i)
                  , 'o(%d).0' % i
                  , Pin.Direction.NORTH
                  , Pin.PlacementStatus.FIXED
                  , metal3
                  , l( 100.0*i + 25.0 ) , l( 0.0 )  # Position.
                  , l( 2.0 )            , l( 2.0 )  # Size.
                  )

      Pin.create( cell.getNet('rst')
                , 'p_reset.0'
                , Pin.Direction.WEST
                , Pin.PlacementStatus.FIXED
                , metal2
                , l(   0.0 )
                , l( 140.0 )
                , l(   2.0 )
                , l(   2.0 )
                )
  UpdateSession.close()

  if editor: editor.setCell( cell )

  print "editor", editor, dir(editor)

  #af.saveCell( cell, CRL.Catalog.State.Views )
  plugins.RSavePlugin.ScriptMain( **kw )

  return 0

################# SNX
#

def alu_hier_route(**kw):

  editor = None
  if kw.has_key('editor') and kw['editor']:
    editor = kw['editor']

  db = DataBase.getDB()
  print db, dir(db)
  metal2 = DataBase.getDB().getTechnology().getLayer( 'metal2' )
  metal3 = DataBase.getDB().getTechnology().getLayer( 'metal3' )

  cell = af.getCell( 'alu_hier', CRL.Catalog.State.Logical )
  if not cell:
    print '[ERROR] Unable to load cell "snx.vst", aborting .'
    return False
  kw[ 'cell' ] = cell

  ab = Box( l(    0.0 )
          , l(    0.0 )
          , l( 1000.0 )
          , l(  600.0 ) )

  cell.setAbutmentBox( ab )

  if editor: editor.setCell( cell )

  print "editor", editor, dir(editor)

  if True:
      etesian = Etesian.EtesianEngine.create(cell)
      #etesian.place()

      katana = Katana.KatanaEngine.create(cell)
      katana.digitalInit          ()
      katana.runNegociatePreRouted()
      print dir(katana)
      katana.runGlobalRouter      (0)
      katana.loadGlobalRouting    ( Anabatic.EngineLoadGrByNet )
      katana.layerAssign          ( Anabatic.EngineNoNetLayerAssign )
      katana.runNegociate         ( Katana.Flags.NoFlags )
      katana.finalizeLayout       ()
      print dir(katana)
      success = katana.getSuccessState()
      katana.destroy()
  else:
      success = 0

  #af.saveCell( cell, CRL.Catalog.State.Views )
  plugins.RSavePlugin.ScriptMain( **kw )

  print "cell", cell, dir(cell)
  c = cell.getComponents()
  print "components", c, dir(c)
  for child in cell.getInstances():
      print "child", child

  return success


if __name__ == '__main__':
  success      = add()
  #success      = sub()
  #success      = alu_hier_place()
  #success      = alu_hier_route()
  shellSuccess = 0
  if not success: shellSuccess = 1

  sys.exit( shellSuccess )


