#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen import Signal, Module, Elaboratable
from nmigen.back.pysim import Simulator, Delay
from nmigen.cli import rtlil

from ieee754.part.partsig import PartitionedSignal
from ieee754.part_mux.part_mux import PMux


# XXX this is for coriolis2 experimentation
class TestLS(Elaboratable):
    def __init__(self, width, partpoints):
        self.partpoints = partpoints
        self.a = PartitionedSignal(partpoints, width, name="a")
        self.b = PartitionedSignal(partpoints, width, name="b")
        self.ls_output = Signal(width) # left shift
        self.dummy_output = Signal(width)
        self.dummy_mask = Signal(self.partpoints.shape()) 

    def elaborate(self, platform):
        m = Module()
        comb = m.d.comb
        sync = m.d.sync
        self.a.set_module(m)
        self.b.set_module(m)
        # left shift
        wid = len(self.partpoints)
        sync += self.dummy_mask.eq(self.partpoints) 
        sync += self.dummy_output.eq(self.b.sig + self.a.sig) # stops ignored
        sync += self.ls_output.eq(self.a << self.b)
        ppts = self.partpoints

        return m

    def ports(self):
        return [self.a.sig, self.b.sig,
                self.ls_output,
                self.dummy_output,
                self.dummy_mask]


# XXX this is for coriolis2 experimentation
class TestAddMod2(Elaboratable):
    def __init__(self, width, partpoints):
        self.partpoints = partpoints
        self.a = PartitionedSignal(partpoints, width, name="a")
        self.b = PartitionedSignal(partpoints, width, name="b")
        self.add_output = Signal(width)
        self.ls_output = Signal(width) # left shift
        self.sub_output = Signal(width)
        self.carry_in = Signal(len(partpoints)+1)
        self.add_carry_out = Signal(len(partpoints)+1)
        self.sub_carry_out = Signal(len(partpoints)+1)
        self.neg_output = Signal(width)

    def elaborate(self, platform):
        m = Module()
        comb = m.d.comb
        sync = m.d.sync
        self.a.set_module(m)
        self.b.set_module(m)
        # add
        add_out, add_carry = self.a.add_op(self.a, self.b,
                                           self.carry_in)
        sync += self.add_output.eq(add_out)
        sync += self.add_carry_out.eq(add_carry)
        # sub
        sub_out, sub_carry = self.a.sub_op(self.a, self.b,
                                           self.carry_in)
        sync += self.sub_output.eq(sub_out)
        sync += self.sub_carry_out.eq(sub_carry)
        # neg
        sync += self.neg_output.eq(-self.a)
        # left shift
        sync += self.ls_output.eq(self.a << self.b)
        ppts = self.partpoints

        return m

    def ports(self):
        return [self.a.sig, self.b.sig,
                self.add_output,
                self.ls_output,
                self.sub_output,
                self.carry_in,
                self.add_carry_out,
                self.sub_carry_out,
                self.neg_output]


