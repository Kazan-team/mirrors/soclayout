#!/usr/bin/env python

from helpers import l, u, n


chip = { 'pads.ioPadGauge' : 'pxlib',

      #   | Instance  | Pad   | To Core          | From Core     | Enable    |
       'pads.instances' :[
        # "a" input.
        [ 'p_a0'    , 'a(0)', 'a(0)' ],
        [ 'p_a1'    , 'a(1)', 'a(1)' ],
        [ 'p_a2'    , 'a(2)', 'a(2)' ],
        [ 'p_a3'    , 'a(3)', 'a(3)' ],
        # "b" input.
        [ 'p_b0'    , 'b(0)', 'b(0)' ],
        [ 'p_b1'    , 'b(1)', 'b(1)' ],
        [ 'p_b2'    , 'b(2)', 'b(2)' ],
        [ 'p_b3'    , 'b(3)', 'b(3)' ],
        # "f" output.
        [ 'p_f0'    , 'f(0)',   'f(0)' ], # , 'f_oe' ],
        [ 'p_f1'    , 'f(1)',   'f(1)' ], # , 'f_oe' ],
        [ 'p_f2'    , 'f(2)',   'f(2)' ], # , 'f_oe' ],
        [ 'p_f3'    , 'f(3)',   'f(3)' ], # , 'f_oe' ],
        ],
        'pads.south'      :
            [ 'p_a1', 'p_vddick_0', 'p_vssick_0' , 'p_a0'       ],
       'pads.east'       :
            [ 'p_a2', 'p_a3'       , 'p_b3'        , 'p_b2'       ],
       'pads.north'      :
            [ 'p_b1', 'p_vddeck_0', 'p_b0'        , 'p_vsseck_0', 'rst' ],
       'pads.west'       :
            [ 'p_f3', 'p_f2'       , 'p_clk_0', 'p_f1'       , 'p_f0' ],
       'core.size'       : ( l( 2500), l( 2500) ),
       'chip.size'       : ( l(4200), l(4200) ),
       'pads.useCoreSize'  : True,
       'chip.clockTree'  : True,
       }

